
:build
echo.
echo Building ...

@ECHO ON
python txtmerge.py source raw.txt
txt2gam.exe raw.txt berlin.qsp > nul

echo Build complete
@ECHO OFF
start Qqsp.exe.lnk %~dp0berlin.qsp
:exit