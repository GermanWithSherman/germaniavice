#!/usr/bin/env python
# usage: txtmerge.py <input_dir> <output_file_name> 
# does the exact opposite of txtsplit.py

import os
import sys
import re
import io 

assert len(sys.argv) == 3, "usage:\ntxtmerge.py <input_dir> <output_file_name>"

inputdir = str(sys.argv[1])
outputfilename = str(sys.argv[2])

with io.open(outputfilename, 'w', encoding='utf-16', newline='\r\n') as outputfile:

    #for file in os.listdir(inputdir):
    for root, subdirs, files in os.walk(inputdir):
        for filename in files:
            filepath = os.path.join(root,filename)
            with io.open(filepath, 'rt', encoding='utf-8') as inputfile:
                text = inputfile.read()

                # EOF
                if text[-1] != u'\n':
                    text += u'\n\n'

                outputfile.write(text)

